#!/usr/bin/python27
import Queue
import thread
import threading
from os import stat
class AsyncFileIO():
    num_threads = int
    tasks = Queue.Queue()
    remainder_lock = thread.allocate_lock()
    list_lock = thread.allocate_lock()
    remainder_list = []
    thread_lists = []
    thread_numbers = {}
    remainingbytes = None
    last = None
    chunk = None
        
    def __init__(self, threads):
        self.num_threads = threads
    
    def async_read_lines(self, filename):
        size = stat(filename).st_size
        self.chunk = size/self.num_threads
        self.remainingbytes = size % self.chunk
        #calculate thread numbers and setup queue
        for i in range(self.num_threads):
            #initialize lists to be correct length
            self.thread_lists.append([])
            self.remainder_list.append("")
            #IO error here is not recoverable
            fd = open(filename, 'r')
            fd.seek(i * size)
            self.thread_numbers[str(i*size)] = i
            self.tasks.put(fd)        
        self.last = (self.num_threads - 1) * self.chunk
        #init threads
        for i in range(self.num_threads):
            t = threading.Thread(target=self._read_lines)
            #python handles cleanup when program exits
            t.daemon = True
            t.start()
            print "spawn thread:", i
        self.tasks.join()
        merged_list = []
        for i in range(self.num_threads):
            print self.thread_lists[i]
            self.thread_lists[i][-1] += self.remainder_list[i]
            merged_list += self.thread_lists[i]
        return merged_list
    
    #converts position into thread number
    def _thread_number(self, pos):
        return self.thread_numbers[str(pos)]
    
    def is_last(self, pos):
        if self.last == pos:
            return True
        return False
    
    #===========================================================================
    # thread function for async_reads_lines
    # Expects a file descriptor, reads from the queue how much it should write, 
    # it then reads that many lines and tries to put it's lines onto the class list 
    # for lines read[thread].
    #===========================================================================
    def _read_lines(self):
        fd = self.tasks.get()
        pos = fd.tell()
        if self.is_last(pos):
            length = self.chunk + self.remainingbytes
        else:
            length = self.chunk
        thread_n = self._thread_number(pos)
        lines = []
        line = ""
        for i in range(length):
            line += fd.read(1)
            if len(line) > 0:
                if line[-1] == '\n':
                    lines.append(line)
                    line = ""
        #if we didnt read \n
        if len(line) > 0:
            try:
                if line[-1] != "":
                    self.remainder_lock.acquire()
                    self.remainder_list[thread_n] = line
                    self.remainder_lock.release()
                    self.list_lock.acquire()
                    self.thread_lists[thread_n] = lines
                    self.list_lock.release()
            except:
                print line
        self.tasks.task_done()
        fd.close()
        
mytest = AsyncFileIO(2)
print mytest.async_read_lines('testme')